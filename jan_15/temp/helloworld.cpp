// Put includes here

#include<iostream>

// I want to output to the console the phrase "Hello World"
// return an int from the main starting point of the program.
int main()
{
  std::cout<< "Hello World, this is Jane Miceli" << std::endl;

  // return 0 here to tell us everything ran OK
  return 0;
}
