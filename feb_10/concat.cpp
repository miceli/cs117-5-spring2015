#include<string>
#include<iostream>

using namespace std;

int main()
{
  string a = "apple";
  string b = "blue";
  string space = " ";
  string c = b+a;
  string d = c.substr(0,4);
  string e = c.substr(1,4); //"blue apple"
  cout << "The string c is " << c << endl; 
  cout << "The string d is " << d << endl;
  cout << "The string e is " << e << endl;
  int first = 5;
  double second = 3.14;
  cout << "The multiplication of the two are: " << first * second << endl;
  return 0;
}