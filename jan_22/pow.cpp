// JAN 27
// Put includes here
#include<iostream>
#include<string> // Added to use variable type string
#include<iomanip>
#include<cmath>

using namespace std;

int main()
{
  double result; // defaults to 0.0;
  cout << "Please enter a number:" ;
  double entered_number;
  cin >> entered_number;
  result = pow(entered_number, 3);
  cout << "The cube is: " << result << endl;
  return 0;
}
