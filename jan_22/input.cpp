// JAN 27
// Put includes here
#include<iostream>
#include<string> // Added to use variable type string
#include<iomanip>
using namespace std;

int main()
{
  //int return_var; //  declaring the type of variable integer and naming it return_var
  //return_var = 0; // initalizing the variable return var to zero
  cout << "Please enter your first name: ";
  string first_name;
  cin >> first_name;
  cout << "Please enter your last name: ";
  string last_name;
  cin >> last_name;
  string output_message;
  output_message = "Hello World, ";
  string message_with_name =  output_message + "my name is " + first_name + " " +  last_name + ".";
    cout << message_with_name << endl;
  /*
  
  cout << message_with_name << endl;
  cout << output_message << endl;
  output_message.append("my name is ");
  cout << output_message << endl;
  output_message.append(first_name);
  cout << output_message << endl;
  output_message.append(".");
  cout << output_message << endl;

  int num_of_sections = 5;
  double num_of_students = 25;
  std::cout<< "num_of_sections: " << num_of_sections << std::endl;
  std::cout<< "num_of_students: " << num_of_students << std::endl;
  // multiply with * the number of students and the number of sections
  // 125 = 5 X 25
  int num_cs117 = num_of_sections * num_of_students; //2.4
  
  //output the result
  std::cout<< "num_cs117: " << num_cs117 << std::endl;
  double divisor =4;
  double result = num_of_students / divisor;
  std::cout<< "result: " << result << std::endl;
  return return_var; //using variable
*/  
  return 0;
}
