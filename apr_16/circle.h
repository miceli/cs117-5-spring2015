#ifndef CIRCLE_H
#define CIRCLE_H

using namespace std;

class Circle {
  int radius;
public:
  Circle();
  Circle(int);
  void set_values(int);
  double circumference() {return 2*radius*3.14145926;}
  
};

#endif


