
#include <string>
#include <iostream>

using namespace std;

int main(int argc, char* argv[])
{

  string input1, input2;
  cout << "The number of arguments passed in are: " << argc << endl;
  cout << "argv[0]: " << argv[0] << endl;
  input1 = argv[1];
  cout << "argv[1]: " << argv[1] << endl;
  if (argc == 3)
  {
     input2 = argv[2];
     cout << "argv[2]: " << argv[2] << endl;
  }
	//
	// get all of the inputs...
	//
// std::string input = argv[1];
	return 0;
}
