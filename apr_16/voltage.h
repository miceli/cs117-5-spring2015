
#include <iostream>
using namespace std;

// Comments that say what this method does.
void read(ifstream& input, double* resistance, double* current);
void calculate(double* voltage, double* resistance, double* current);
void print(double* voltage, double* resistance, double* current);