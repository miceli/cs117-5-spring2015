#include <iostream>
#include "rectangle.h"
#include "circle.h"
using namespace std;
int main () {
  Rectangle rect;
  rect.set_values(3,4);
  Rectangle rect1;
  rect1.set_values(6,12);
  cout << "rect.area: " << rect.area() << endl;
  cout << "rect1.area: " << rect1.area() << endl;
  Circle circ;
  circ.set_values(4);
  Circle circ1(5);
  Circle circ2;
  cout << "circ.circumference: " << circ.circumference() << endl;
  cout << "circ1.circumference: " << circ1.circumference() << endl;
  cout << "circ2.circumference: " << circ2.circumference() << endl;
  return 0;
}
