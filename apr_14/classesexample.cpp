#include <iostream>
using namespace std;

// used in rectange.h
class Rectangle {
  // class data object
  int width, height;
public:
  // methods/functions that manipulate data
  void set_values (int,int);
  int area() {return width*height;}
};

// used in rectange.cpp
void Rectangle::set_values (int x, int y) // method in class
{
  width = x;
  height = y;
}

// used in main.cpp
int main () {
  Rectangle rect;  // instansiate new object of type class Rectagle
  rect.set_values(3,4);
  cout << "area: " << rect.area()<< endl;
  return 0;
}

