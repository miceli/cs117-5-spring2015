// setprecision example
#include <iostream>     // std::cout, std::fixed
#include <iomanip>      // std::setprecision

using namespace std;
//double f = 13;
double addition(double f, double g)
{
 cout << "I am in the addition() function" << endl; 
 cout << std::setprecision(5) << f << '\n';
 double value = 1.0;
 cout << "value is: " << value << '\n';
 cout << "f is: " << f << '\n';
 cout << "f = f + value" << endl;
 f = f + value;
 cout << "value is: " << value << '\n';
 cout << "f is: " << f << '\n';
 cout << "f += value" << endl;
 f += value;
 cout << "value is: " << value << '\n';
 cout << "f is: " << f << '\n';
 cout << "f ++" << endl;
 f++; // will only increment by 1
 cout << "value is: " << value << '\n';
 cout << "f is: " << f << '\n';
 return f;
}

int main () {
  double outofbounds = 51.00 ;
  double f = 13.14159;
  //double min = 0.0;
  double max = 50.00;
  
  if(outofbounds == max){
     cout << "outofbounds is equal to max" << endl;
     cout << "orange is the cooler color" << endl;
  }
  else if(outofbounds > max){
     cout << "not a valid number" << endl; 
     // return 0;
  }  
  double temp = (b*addition(f,g))/(a-addition(f,g));
  cout << std::setprecision(5) << f << '\n';
  cout << std::setprecision(9) << f << '\n';
  cout << std::setprecision(5) << g << '\n';
  cout << std::setprecision(9) << g << '\n';
  cout << std::setprecision(5) << f << '\n';
  cout << std::setprecision(9) << f << '\n';
  return 0;
}

