#include <iostream>
#include <string>
#include "rectangle.h"
#include "circle.h"

using namespace std;

int main () 
{
    
    Circle circ;          // Instantiates new object of type Circle using default constructor
                          // Constructor #1
    circ.set_value(4);    // Sets values using a mutator of the class
    Circle circ1(5);      // Uses Circle(int) constructor
                          // Constructor #2
    Circle circ2;         // Instantiates new object of type Circle
    cout << "circ.circumference: " << circ.circumference() << endl;
    cout << "circ1.circumference: " << circ1.circumference() << endl;
    cout << "circ2.circumference: " << circ2.circumference() << endl;
    circ.print("circ");
    circ1.print("circ1");
    circ2.print("circ2");
    circ2.set_name("circ2");
    cout << circ2.get_name() <<endl; //accessor
    return 0;
}