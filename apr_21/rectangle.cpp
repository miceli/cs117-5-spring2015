#include "rectangle.h"

using namespace std;

void Rectangle::set_values (int x, int y)
{
    width = x;
    height = y;  
}

int Rectangle::area()
{  
    return width*height;  
}

double Rectangle::circumference() 
{
    return 2*width+2*height;
}

double Rectangle::perimeter() 
{
    return circumference();
}