#ifndef CIRCLE_H
#define CIRCLE_H
#include <iostream>

//#include "shape.h"
using namespace std;

class Circle//: public Shape 
{
 public:
    Circle(); //Constructor 1 //returns void, this is our default constructor
    Circle(int); //Constructor 2 
    Circle(string); //Constructor 3 
    Circle(int,string); //Constructor 4
    void set_value(int);
    void print();
    void print(string);
    int get_value();
    double circumference();
    double area();
    string get_name(){return name;};
    void set_name(string n){name=n;};
 private:
    int radius;  // data member
    string name; //data member default set to "circle"
    double perimeter();
};

#endif