#include "circle.h"
#include <iostream>
#include "shape.h"

using namespace std;

Circle::Circle() //default constructor
{
    radius=1;  
    name="circle"; 
}

Circle::Circle(int r){
    radius=r;    
}

Circle::Circle(string x){
    name=x;    
}

Circle::Circle(int r, string x){
    name=x; 
    radius=r;   
}

void Circle::set_value(int x) {
    radius = x;   
}

int Circle::get_value() {
    return radius;   
}

double Circle::circumference() {
    return 2*radius*3.14145926;    
}

double Circle::area() {
    return radius*radius*3.14145926;   
}

double Circle::perimeter() {
    return circumference();   
}

void Circle::print() {
    cout << "Circle has a radius of " << radius << " which means an area of " << circumference() << " and a perimeter of " << area() << endl;
}

void Circle::print(string name) {
    cout << "Circle " << name <<" has a radius of " << radius << " which means an area of " << circumference() << " and a perimeter of " << area() << endl;
}