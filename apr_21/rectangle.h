#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "shape.h"

using namespace std;

class Rectangle: public Shape  
{   
 public:
    void set_values (int,int);
    int area();
    double circumference();
    double perimeter();
 private:
    int width, height;
};

#endif