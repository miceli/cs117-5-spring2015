#ifndef STUDENT_H
#define STUDENT_H

#include <iostream>

using namespace std;

class Student
{
public:
	/*

	Constructors...
	Note: There is no default constructor for Student, it wouldn't make sense in 
	this implementation.

	*/
	Student(string fname, string lname, string major, double totalUnits);
	Student(string fname, string lname, double totalUnits);
	Student(string fname, string lname);
	/*

	Mutators...

		Update the sudent's total units with param 'units' and update student
		class standing accordingly (Freshman...Senior)
	*/
	void updateTotalUnits(double units);
	/*
		Update the sudent's semester units with param 'units'
	*/
	void updateSemesterUnits(double units);
	/*
		Complete the semester...
		Update the sudent's total units with student's semester units, 
			clear out student's semester units, update student's class standing.
	*/
	void completeSemester();
	/*
		Update the sudent's major with param 'newMajor'
	*/
	void updateMajor(string newMajor);
	/*

	Assessors...

		Returns the student's total units
	*/
	double getStudentTotalUnits();
	/*
		Returns the student's current units taken in the semester
	*/
	double getStudentCurrentUnits();
	/*
		Returns the student's name
	*/
	string getStudentName();
	/*
		Returns the student's major
	*/
	string getStudentMajor();
	/*
		Returns the student's class standing
	*/
	string getStudentClass();
	/*
		Returns the student's ID
	*/
	int getStudentID();
	/*
		Prints out the student's record
	*/
	void printStudent();
	/*
	destructor...
	*/

private:
	int studentId;
	string major;
	string fname;
	string lname;
	string whatClass;
	double unitsThisSemester;
	double totalUnits;
	int lastID;				// this is the last ID in use.  This variable is 'shared' between all objects; i.e., there
									// is only 1 copy of lastID for all students;
	/*
	Private mutators only for execution WITHIN THE CLASS; i.e., these are NOT
	visible to the outside, nor are they 'callable'...

		Returns the next available student ID that isn't being used.
		NOTE: this mutator is in the 'private' section as no one should
		be calling this routine from the outside.
	*/
	int getID();
	/*
		Returns the students class level: Freshman, Sophomore, Junior, Senior depending
		on a 30 credit class level; i.e., 0-29 is a Freshman, 30-59 is a Sophomore,
		60-89 is a Junior, and > 90 is a Senior.

		Note:this routine is called whenever the student's totalUnits are updated.
	*/
	string setClass(double units);

};
#endif