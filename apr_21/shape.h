#ifndef SHAPE_H
#define SHAPE_H

using namespace std;

class Shape 
{ 
 public:
  int area();
  int perimeter();

 protected:
  string name;
};
#endif
