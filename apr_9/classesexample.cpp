// classes example
#include <iostream>
using namespace std;

// class definition
class Rectangle {
  // class data object
  int width, height;
public:
  // methods/functions that manipulate data
  void set_values (int,int);
  int area() {return width*height;}
};

void Rectangle::set_values (int x, int y) // method in class
{
  width = x;
  height = y;
}

int main () {
  Rectangle rect;  // instansiate new object of type class Rectagle
  rect.set_values (3,4);
  cout << "area: " << rect.area()<< endl;
  return 0;
}

