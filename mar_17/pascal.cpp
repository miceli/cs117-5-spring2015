// S2015MT2.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <iostream>

using namespace std;


int main()
{
	const int ARRAYSIZE = 10;
	double values[5] = { 1.0, 2.0, 3.0, 4.0 };
	int values2[] =  { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	double average, sum = 0;
	int j = 0;
	while (j < ARRAYSIZE)
	{
		sum += values2[j];
		j++;
	}
	average = sum / ARRAYSIZE;
	cout << "Average of values2 array is :" << average << endl;
	//
	//
	//
	//

	average = 0; sum = 0;
	for (int i = 0; i < ARRAYSIZE; i++)
		sum += values2[i];
	average = sum / ARRAYSIZE;
	cout << "Average of values2 array is :" << average << endl;

	//
	// pointers...
	//
	int i = 7;
	j = 12;
        double *aPtr = values;
	int *values2Ptr = values2;
	int *iPtr = &i;
	int *jPrt = &j;
	cout << "i == " << i << endl;
	cout << "j == " << j << endl;
	cout << "&i == " << &i << endl;
	cout << "&j == " << &j << endl;
	cout << "iPtr == " << iPtr << endl;
	cout << "*iPtr == " << *iPtr << endl;
	cout << "&iPtr == " << &iPtr << endl;
	cout << "*jPtr - *iPtr == " << *jPrt - *iPtr << endl;
	cout << "iPtr + 1 == " << iPtr + 1 << endl;
 	cout << "aPtr == " << aPtr << endl;
 	cout << "aPtr + 2 == " << iPtr + 2 << endl; 
        cout << "values2Ptr == " << values2Ptr << endl;
        cout << "values2Ptr == " << values2Ptr + 2 << endl;
	return 0;
}
