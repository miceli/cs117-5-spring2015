#include<iostream>

using namespace std;

int main()
{
 int start = 0;
 int end = 20;
 int start1 = 0;
 
 while ( start < end && start1 < 5 )
 {
   cout << "The value of start is: " << start << endl;
   cout << "The value of start1 is: " << start1 << endl;
   start++;
   start1++;
   // Another way to break out of the loop
   //if (5 == start)
   //{
   //   break;
   //}
 }
 
 for (int i =0; i<20; i++)
 {
   cout << "The value of i is: " << i << endl;
 }
 
 return 0;
}